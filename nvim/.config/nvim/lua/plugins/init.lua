return {
  'williamboman/mason-lspconfig.nvim',
  'nvim-lua/plenary.nvim',
  'tpope/vim-fugitive',
  'tpope/vim-rhubarb',
  'vimpostor/vim-tpipeline',
  'tpope/vim-commentary',
  {
    'lewis6991/gitsigns.nvim',
    config = function ()
      require('gitsigns').setup {
        signs = {
          add = { text = '+' },
          change = { text = '~' },
          delete = { text = '_' },
          topdelete = { text = '‾' },
          changedelete = { text = '~' },
        },
      }
    end
  },

  {
    "catppuccin/nvim", name = "catppuccin",
    config = function()
      vim.cmd('colorscheme catppuccin')
    end
  },

  {
    'lukas-reineke/indent-blankline.nvim',
    config = function ()
      require('indent_blankline').setup {
        char = '┊',
        show_trailing_blankline_indent = false,
      }
    end
  },
  'tpope/vim-sleuth',
  {
    "simrat39/symbols-outline.nvim",
    keys = { { "<leader>cs", "<cmd>SymbolsOutline<cr>", desc = "Symbols Outline" } },
    config = true,
  },

  'jose-elias-alvarez/typescript.nvim'
}
